"""hackaton URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from rest_framework import routers
from usermanage import views as u_views
from reserve import views as r_views

router = routers.DefaultRouter()
router.register(r'users', u_views.UserViewSet)
router.register(r'groups', u_views.GroupViewSet)
router.register(r'cordons', r_views.CordonViewSet, basename='cordons')
router.register(r'schedule', r_views.ScheduleViewSet, basename='schedule')
router.register(r'wiki', r_views.WikiViewSet, basename='wiki')
router.register(r'task', r_views.TaskViewSet, basename='task')


urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
]
