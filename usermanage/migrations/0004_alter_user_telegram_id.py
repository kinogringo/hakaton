# Generated by Django 3.2.9 on 2021-12-04 08:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermanage', '0003_auto_20211204_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='telegram_id',
            field=models.IntegerField(db_index=True, default=0, unique=True, verbose_name='Telegram ID'),
        ),
    ]
