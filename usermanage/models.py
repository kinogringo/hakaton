from django.db import models

from django.contrib.auth.models import AbstractUser
from reserve import models as reserve

class User(AbstractUser):
    class Statuses(models.IntegerChoices):
        NORIGISTER = 0, "Не зарегистрирован"
        NOPLACE = 1, "Кордон не выбран"
        NOREQUEST = 2, "Заявка не подана"
        NOCONTRACT = 3, "Нет договора"
        SCHEDULE = 4, "Выбран график"
        ARRIVED = 5, "Прибыл"
        
    telegram_id = models.IntegerField("Telegram ID", unique=True, db_index=True, null=False, default=0)
    status = models.SmallIntegerField("Status", choices=Statuses.choices, default=Statuses.NORIGISTER, null=False)
    schedule = models.ForeignKey(reserve.Schedule, blank=True, null=True, related_name='users', on_delete=models.SET_NULL)
    task = models.OneToOneField(reserve.Task, blank=True, null=True, related_name='user', on_delete=models.SET_NULL)
    wikies = models.ManyToManyField(reserve.Wiki, related_name='users', through=reserve.Stage)
    cordon = models.ForeignKey(reserve.Cordon, blank=True, null=True, related_name='users', on_delete=models.SET_NULL)