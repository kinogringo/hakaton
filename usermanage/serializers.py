from django.contrib.auth.models import Group
from rest_framework import serializers

from django.contrib.auth import get_user_model
from reserve.models import Schedule
User = get_user_model()

from reserve.serializers import (
    CordonSerializer,
    ScheduleUserSerializer,
    TaskSerializer,
    StageSerializer,
)

class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(lookup_field='telegram_id', view_name='user-detail')
    cordon = CordonSerializer(read_only=True)
    schedule = ScheduleUserSerializer(read_only=True)
    task = TaskSerializer(read_only=True)
    stages = StageSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = [
            'url', 'telegram_id', 
            'username', 'email', 'status',
            'cordon', # cordon: {'url', , 'name', 'link'}
            'schedule', # schedule: {'pk', 'from_date', 'to_date', 'description', 'cordon'}
            'task', # task: {'pk', 'user', 'schedule', 'description'}
            'stages' # stages: {'pk', 'status'}
        ]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


# class TrackSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Track
#         fields = ['order', 'title', 'duration']

# class AlbumSerializer(serializers.ModelSerializer):
#     tracks = TrackSerializer(many=True)

#     class Meta:
#         model = Album
#         fields = ['album_name', 'artist', 'tracks']

#     def create(self, validated_data):
#         tracks_data = validated_data.pop('tracks')
#         album = Album.objects.create(**validated_data)
#         for track_data in tracks_data:
#             Track.objects.create(album=album, **track_data)
#         return album
