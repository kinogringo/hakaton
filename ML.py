import numpy as np
import pandas as pd
from catboost import CatBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
import spacy

import pickle
from pathlib import Path


class Model:
    def __init__(self):
        # base models
        self.lgr = LogisticRegression()
        self.cbc = CatBoostClassifier(random_state=42, silent=True)

        # desicion making model in the end
        self.final_model = CatBoostClassifier(random_state=42, silent=True)
        
        # feature transformers basis
        self.bow = CountVectorizer()
        self.nlp = spacy.load("ru_core_news_lg")
        self.text2vector = lambda x: self.nlp(x).vector

        # feature transformers high level
        self.nlp_transform = lambda x: np.vstack(pd.Series(x).map(self.text2vector)).astype(np.float32)
        self.bow_transform = lambda x: self.bow.transform(x).astype(np.float32)
        self.final_transform = lambda x: np.hstack((self.lgr.predict_proba((nlp_features := self.nlp_transform(x))),\
                                                    self.cbc.predict_proba(self.bow_transform(x))))

        self.fitted_attributes = ['lgr', 'cbc', 'final_model', 'bow']

    def load(self, folder):
        folder = Path(folder)
        
        for name in self.fitted_attributes:
            with open(folder / f"{name}.pkl", 'rb') as f:
                self.__setattr__(name, pickle.load(f))

    def save(self, folder):
        folder = Path(folder)
        if not folder.exists(): folder.mkdir()

        for name in self.fitted_attributes:
            with open(folder / f"{name}.pkl", 'wb') as f:
                pickle.dump(self.__getattribute__(name), f)

    def fit(self, Xtrain, ytrain):
        # train X transformation
        bow_train = self.bow.fit_transform(Xtrain).astype(np.float32)
        nlp_train = self.nlp_transform(Xtrain)

        # fitting base models
        self.lgr.fit(nlp_train, ytrain)
        self.cbc.fit(bow_train, ytrain)

        # fitting desicion making model in the end
        self.final_model.fit(self.final_transform(Xtrain), ytrain)
    
    def predict(self, Xtest):
        return self.final_model.predict(self.final_transform(Xtest)).flatten()

    def predict_proba(self, Xtest):
        return self.final_model.predict_proba(self.final_transform(Xtest))

    def predict_kbest(self, Xtest, k=3):
        preds = self.predict_proba(Xtest)
        return [[self.final_model.classes_[y[1]] for y in sorted(((x, i) for i, x in enumerate(row)), reverse=True)[:k]] for row in preds]

    def score(self, Xtest, ytest):
        return np.mean(self.predict(Xtest) == ytest)

    def score_kbest(self, Xtest, ytest, k=3):
        preds = self.predict_kbest(Xtest, k=k)
        return np.mean([y in x for x, y in zip(preds, ytest)])