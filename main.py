import logging
from typing import Dict
import requests
from requests.auth import HTTPBasicAuth

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
)

from ML import Model

model = Model()
model.load('./weights_aug_only')

from enum import IntEnum

from telegram.keyboardbutton import KeyboardButton

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)
TOKEN = '5053928973:AAHfHqvywUsegYvqFCHv8YJQrOowcnHj6D8'
LOGIN, PSWD = 'admin', 'admin'
REPLYINGFIO, AWAITSTARTINFO, REPLYINGCORDONS, CHOOSING, TYPING_REPLY, TYPING_CHOICE = range(6)
apiUrl = 'http://127.0.0.1:8000'

class Status(IntEnum):
    NORIGISTER = 0
    NOPLACE = 1
    NOREQUEST = 2
    NOCONTRACT = 3
    SCHEDULE = 4
    ARRIVED = 5

class Mixin:
    
    @classmethod
    def telegram_id(self, update, *args, **kwargs):
        return update.message.from_user.id
    
    @classmethod
    def storage(self, update, context):
        return context.user_data

    @classmethod
    def data(self, update, context):
        return update.message.text

    @classmethod
    def method(self, method, path, *args, **kwargs):
        response = getattr(requests, method)(apiUrl + path, *args, auth=HTTPBasicAuth(LOGIN, PSWD), **kwargs)
        if response.status_code // 100 != 2: #200/201...
            return None
        return response.json()

    @classmethod
    def message(self, update, msg, *args, reply_markup=ReplyKeyboardRemove(), **kwargs):
        return update.message.reply_text(msg, *args, reply_markup=reply_markup, **kwargs)

class Node(Mixin):
    _registry = {}
    WIKI = 'wiki'
    MENU = 'menu'
    PICK = 'pick'
    SEND = 'send'
    EMPTY = ''

    _types = [WIKI, PICK, MENU, SEND, EMPTY]
    _dispatch = [
        "get_user_data",
        "get_server_data",
        "process_server_data",
        "process_user_data",
        "send_server_data",
        "send_user_data",
    ]

    def state(self):
        return abs(hash(self.name))

    def get_state(self, name):
        for k, v in self._registry.items():
            if v.name == name:
                return v
        return None

    def __init__(self, name, values=[], **kwargs):
        self.name = name
        self.values = values
        if isinstance(values, (list, tuple)):
            for v in values:
                v.parent = self
        elif isinstance(values, (dict)):
            for v in values.values():
                v.parent = self
        for key, value in kwargs.items():
            setattr(self, key, value)
        
        self._registry[self.state()] = self 

    def self_asset(self):
        return {
            self.state(): [
            self.handler(
                self.filter,
                self.process,
            )
        ]}

    def asset(self):
        asset = self.self_asset()
        if isinstance(self.values, (list, tuple)):
            for inner in self.values:
                if not isinstance(inner, Node): continue
                a = inner.asset()
                for k, v in a.items():
                    if not k in asset:
                        asset[k] = v 
                    else:
                        asset[k] += v
                    asset.update()
        elif isinstance(self.values, (dict)):
            for inner in self.values.values():
                if not isinstance(inner, Node): continue
                a = inner.asset()
                for k, v in a.items():
                    if not k in asset:
                        asset[k] = v 
                    else:
                        asset[k] += v
                    asset.update()

        return asset

    def intro(self, update, context):
        self.message(update, "intro: {}".format(str(self)))

    def get_server_data(self, update, context):
        self.message(update, "get_server_data: {}".format(str(self)))
        
    def get_user_data(self, update, context):
        self.message(update, "get_user_data: {}".format(str(self)))

    def process_server_data(self, update, context):
        self.message(update, "process_server_data: {}".format(str(self)))
        
    def process_user_data(self, update, context):
        self.message(update, "process_user_data: {}".format(str(self)))

    def send_server_data(self, update, context):
        self.message(update, "process_server_data: {}".format(str(self)))
        
    def send_user_data(self, update, context):
        self.message(update, "process_user_data: {}".format(str(self)))

    def get_next_node(self, update, context):
        force_go = getattr(self, 'force_go', None)
        if force_go is not None:
            return self.get_state(force_go)
        if isinstance(self.values, (list, tuple)):
            return self.values[0]
        elif isinstance(self.values, (dict)):
            return self.values[self.storage(update, context)['res']]

    def process(self, update, context):
        self.update = update
        self.context = context
        for node_type in self._types:
            for func_name in self._dispatch:
                func = getattr(self, node_type + '_' + func_name, None)
                if func is None:
                    continue
                func(update, context)

        next_node = self.get_next_node(update, context)

        if isinstance(next_node, Node):
        
            for node_type in self._types:
                func = getattr(next_node, node_type + '_intro', None)
                if func is None:
                    continue
                func(update, context)
            next_node = next_node.state()
        print("go to", self._registry[next_node])
        return next_node

    def __str__(self) -> str:
        return self.__class__.__name__ + ':' + self.name

class MenuBaseMixin:
    # init(name='fff')
    handler = MessageHandler
    filter = Filters.text
    
    def menu_make_table(self, update, context, items):
        split = 3
        l = len(items)
        keyboard = []
        for i in range(l//split + 1):
            keyboard.append([])
            for j in range(split):
                if i*split + j >= l:
                    break
                item = items[i*split + j]
                keyboard[i].append(
                    KeyboardButton(item['name']),
                )
        return ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)

    def menu_intro(self, update, context):
        fields = self.menu_get_fields(update, context)
        if fields is None: return
        markup = self.menu_make_table(update, context, fields)
        self.message(update, "{}:\n".format(self.name), reply_markup=markup)

    def get_next_node(self, update, context):
        return self.values[0].state()

    def menu_get_user_data(self, update, context):
        fields = self.menu_get_fields(update, context)
        if fields is None: return
        data = self.data(update, context)
        for f in fields:
            if f['name'] == data:
                self.storage(update, context)[self.name] = f
                if self.get_state(data):
                    self.force_go = data


class StaticMenuMixin(MenuBaseMixin):
    # init(name='fff', menu_choices={pk, name})
    def menu_get_fields(self, update, context):
        return self.menu_choices

class ConditionMenuMixin(MenuBaseMixin):
    # init(name='fff', menu_choices: {name: status})
    def menu_get_fields(self, update, context):
        try:
            user = self.storage(update, context)['user']
        except:
            return None
        items = list(filter(lambda x: x[1] <= user['status'], self.menu_choices.items()))
        items = [{'pk': x[0], 'name': x[0]} for x in items ]
        return items

class UrlMenuMixin(MenuBaseMixin):
    # init(name='fff', menu_path=lambda self: )
    def menu_get_fields(self, update, context):
        fields = self.method('get', self.menu_path(self))
        if fields is None or fields[0].get('pk', '') == '':
            return None
        
        return fields


class WikiMixin:
    wiki_name = None

    def wiki_intro(self, update, context):
        wiki_get_path = getattr(self, "wiki_get_path", lambda self: "/wiki/{}/".format(self.telegram_id(update, context)))
        wiki = self.method('get', wiki_get_path(self))
        if wiki is None: return None

        results = []
        for w in wiki:
            if w['name'] == self.wiki_name or getattr(self, 'wiki_all', False):
                results.append(w)
                if not getattr(self, 'wiki_all', False):
                    break
        if not results:
            wiki_base = self.method('get', "/wiki/")
            if wiki_base is None: return None
            for w in wiki_base:
                if w['name'] == self.wiki_name or getattr(self, 'wiki_all', False):
                    results.append(w)
                    if not getattr(self, 'wiki_all', False):
                        break
        
        if results:
            for res in results:
                res.pop('pk', None)
                res['name'] += '\n' + res.get('status', '')
                self.message(update,
                    "{}\n"
                    "Ссылка: {}\n"
                    "WIKI: {}\n"
                    "{}".format(*res.values())
                )

class TextEntryMixin:
    # init(entry="sds")
    handler = MessageHandler
    filter = Filters.text

    def pick_intro(self, update, context):
        if hasattr(self, 'entry'):
            self.message(update, self.entry)
        
    def pick_get_user_data(self, update, context):
        data = self.data(update, context)
        self.storage(update, context)[self.name] = data

class FileEntryMixin:
    # init(entry="sds")
    handler = MessageHandler
    filter = Filters.document | Filters.video

    def downloader(self, name):
        self.context.bot.get_file(self.update.message.document).download()

        # writing to a custom file
        with open(name, 'wb') as f:
            self.context.bot.get_file(self.update.message.document).download(out=f)

    def pick_intro(self, update, context):
        if hasattr(self, 'entry'):
            self.message(update, self.entry)
        
    def pick_get_user_data(self, update, context):
        self.downloader(self.filename)

class MLPredictMixin:
    handler = MessageHandler
    filter = Filters.text

    def dispatch_text(self):
        data = self.data(self.update, self.context)
        res = model.predict([data])
        return res

    def pick_process_user_data(self, update, context):
        topic = self.dispatch_text()
        if not isinstance(topic, str):
            topic = ''.join(topic)

        found = False
        for k, m in self._registry.items():
            if topic.lower() in m.name.lower():
                self.force_go = m.name
                found = True
                break
        if not found:
            self.force_go = 'start'
        self.message(update, topic)

class AudioEntryMixin:
    # init(entry="sds")
    handler = MessageHandler
    filter = Filters.audio

    def dispatch_voice(self):

        return 'Рекомендации'

    def pick_intro(self, update, context):
        if hasattr(self, 'entry'):
            self.message(update, self.entry)
        
    def pick_get_user_data(self, update, context):
        next = self.dispatch_voice()
        self.force_go = next



class SenderMixin:
    # init(menu_path=lambda self: ,send_method)
    def send_send_server_data(self, update, context):
        data = self.storage(update, context)['send']
        res = self.method(self.send_method, self.send_path(self), json=data)

class StatusUpdaterMixin:
    # init(next_status)
    def _send_server_data(self, update, context):
        if self.storage(update, context)['user']['status'] < self.next_status:
            res = self.method('patch', "/users/{}/".format(self.telegram_id(update, context)), json={'status': self.next_status})
            if res is not None:
                self.storage(update, context)['user']['status'] = self.next_status

class MainMenu(Node, Mixin, ConditionMenuMixin):
    handler = MessageHandler
    filter = Filters.text
    pass 

class WikiUrlSenderUpdaterPickerMenu(Node, Mixin, WikiMixin, UrlMenuMixin, SenderMixin, StatusUpdaterMixin):
    def send_process_user_data(self, update, context):
        send = self.storage(update, context)[self.name]
        self.storage(update, context)['send'] = {'pk': send['pk']}

class WikiStaticPickerMenu(Node, Mixin, WikiMixin, StaticMenuMixin):
    pass

class WikiStaticUpdaterPickerMenu(Node, Mixin, WikiMixin, StaticMenuMixin, StatusUpdaterMixin):
    pass

class StartNode(Node, Mixin):
    handler = CommandHandler
    filter = 'start'

    def _get_server_data(self, update, context):
        user = self.method('get', "/users/{}/".format(self.telegram_id(update, context)))
        if user is None: 
            self.storage(update, context)['user'] = {}
            self.storage(update, context)['res'] = 0
        else:
            if user['status'] == Status.NORIGISTER:
                self.update_status(update, context, Status.NOPLACE)
            self.storage(update, context)['user'] = user
            self.storage(update, context)['res'] = 1

class EnterText(Node, Mixin, TextEntryMixin):
    pass

class EnterFile(Node, Mixin, FileEntryMixin):
    pass

class VoiceFile(Node, Mixin, AudioEntryMixin):
    filter = Filters.voice

class MLText(Node, Mixin, MLPredictMixin):
    pass


class UserSend(Node, Mixin, TextEntryMixin, SenderMixin, StatusUpdaterMixin):
    def send_process_user_data(self, update, context):
        user = self.storage(update, context)['user']
        user['status'] = Status.NORIGISTER
        user['telegram_id'] = self.telegram_id(update, context)
        user['username'] = self.storage(update, context)['fio']
        user['email'] = self.storage(update, context)['email']
        self.storage(update, context)['send'] = user

node = StartNode("start", values=
    {
        0: EnterText("fio", entry="Введите ФИО", values=[UserSend("email", entry="Ввведите Email", next_status=Status.NOPLACE, force_go='start', send_method='post', send_path=lambda self: "/users/"),]),
        1: MainMenu('main_menu', menu_choices={'Написать вопрос': Status.NOPLACE, 'Задать вопрос': Status.NOPLACE, 'Кордон': Status.NOPLACE, 'Заявка': Status.NOREQUEST, 'Работа': Status.NOCONTRACT, 'Жизнь': Status.SCHEDULE}, values=
            [
                WikiUrlSenderUpdaterPickerMenu('Кордон', next_status=Status.NOREQUEST, wiki_all=True, wiki_get_path=lambda self: '/cordons/', menu_path=lambda self: '/cordons/', force_go='start', send_method='put', send_path=lambda self: "/cordons/{}/".format(self.telegram_id(self.update))),
                WikiStaticPickerMenu('Заявка', wiki_name='Заявка', menu_choices=[{'pk':x,'name':x} for x in ['Видео', 'Рекомендации', 'Справки']], values=
                    [
                        WikiStaticPickerMenu('Видео', wiki_name='Видео', menu_choices=[{'pk':x,'name':x} for x in ['Загрузка видео']], values=
                        [
                            EnterFile('Загрузка видео', entry="Отправьте видео", filename='video', force_go='start')
                        ,]),
                        WikiStaticPickerMenu('Рекомендации', wiki_name='Рекомендации', menu_choices=[{'pk':x,'name':x} for x in ['Загрузка рекомендаций']], values=
                        [
                            EnterFile('Загрузка рекомендаций', entry="Отправьте рекомендации в архиве", filename='recomendations', force_go='start')
                        ,]),
                        WikiStaticPickerMenu('Справки', wiki_name='Справки', menu_choices=[{'pk':x,'name':x} for x in ['Загрузка справок']], values=
                        [
                            EnterFile('Загрузка справок', entry="Отправьте справки", filename='sheets', force_go='Работа')
                        ,]),
                    ]
                ),
                WikiStaticUpdaterPickerMenu('Работа', next_status=Status.NOCONTRACT, menu_choices=[{'pk':x,'name':x} for x in ['График', 'Отчет', 'Задания', 'Прибытие']], values=
                [
                    WikiUrlSenderUpdaterPickerMenu('График', next_status=Status.SCHEDULE, wiki_all=True, wiki_get_path=lambda self: '/schedule/', menu_path=lambda self: '/schedule/', force_go='start', send_method='put', send_path=lambda self: "/schedule/{}/".format(self.telegram_id(self.update))),
                    WikiStaticPickerMenu('Отчет', wiki_name='Отчет', menu_choices=[{'pk':x,'name':x} for x in ['Загрузка отчета']], values=
                    [
                        EnterFile('Загрузка отчета', entry="Отправьте отчет", filename='report', force_go='start')
                    ,]),
                    WikiStaticPickerMenu('Задания', wiki_name='Задания', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                    WikiStaticUpdaterPickerMenu('Прибытие', next_status=Status.ARRIVED, menu_choices=[{'pk':x,'name':x} for x in ['Подписание', 'Подготовка', 'Ожидание', 'Отправка']], values=
                    [
                        WikiStaticPickerMenu('Подписание', wiki_name='Подписание', menu_choices=[{'pk':x,'name':x} for x in ['Загрузка договора']], values=
                        [
                            EnterFile('Загрузка договора', entry="Отправьте договор", filename='report', force_go='start')
                        ,]),
                        WikiStaticPickerMenu('Подготовка', wiki_name='Подготовка', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                        WikiStaticPickerMenu('Ожидание', wiki_name='Ожидание', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                        WikiStaticPickerMenu('Отправка', wiki_name='Отправка', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                        
                    ]),
                ]),
                WikiStaticPickerMenu('Жизнь', menu_choices=[{'pk':x,'name':x} for x in ["Проживание", "Здоровье", "Связь", "Питание", "Контакты"]], values=
                [
                    WikiStaticPickerMenu('Проживание', wiki_name='Проживание', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                    WikiStaticPickerMenu('Здоровье', wiki_name='Здоровье', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                    WikiStaticPickerMenu('Связь', wiki_name='Интернет', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                    WikiStaticPickerMenu('Питание', wiki_name='Питание', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                    WikiStaticPickerMenu('Контакты', wiki_name='Контакты', menu_choices=[{'pk':x,'name':x} for x in ['Понял']], force_go='start'),
                ]),
                VoiceFile('Задать вопрос', entry="Запишите голосовой вопрос"),
                MLText('Написать вопрос', entry="Напишите текстовый вопрос", force_go='start')
            ,]
        )
    }
)


states = node.asset()

def main() -> None:
    updater = Updater(TOKEN)

    dispatcher = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=states[node.state()],
        states=states,
        
        fallbacks=states[node.state()],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
