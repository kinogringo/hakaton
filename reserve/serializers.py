from .models import Cordon, Schedule, Stage, Wiki, Task
from rest_framework import serializers

from django.contrib.auth import get_user_model
User = get_user_model()

class CordonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cordon
        fields = ['pk', 'name', 'link']

class CordonDetailedSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Cordon
        fields = ['pk', 'name', 'link', 'infrastructure', 'general_tasks']

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['pk', 'name', 'link', 'quota', 'description']

class ScheduleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['pk', 'from_date', 'to_date', 'description']

class StageSerializer(serializers.ModelSerializer):
    name = serializers.CharField(read_only=True, source='wiki.name')
    pk = serializers.CharField(read_only=True, source='wiki.pk')
    link = serializers.CharField(read_only=True, source='wiki.link')
    description = serializers.CharField(read_only=True, source='wiki.description')
    class Meta:
        model = Stage
        fields = ['pk', 'name', 'link', 'wiki', 'description', 'status']

class WikiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wiki
        fields = ['pk', 'name', 'link', 'wiki', 'description']

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['pk', 'user', 'schedule', 'description']

