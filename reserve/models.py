from django.db import models
from django.conf import settings

class Cordon(models.Model):
    name = models.CharField("Name", max_length=20)
    link = models.URLField("Link")
    infrastructure = models.TextField("Infrastructure")
    general_tasks = models.TextField("General Tasks")

    def __str__(self):
        return f"<Cordon> {self.name}: {self.link}"

class Task(models.Model):
    schedule = models.ForeignKey('Schedule', related_name='tasks', on_delete=models.CASCADE)
    description = models.TextField("Personal task")
    
    def __str__(self):
        return f"<Task> {self.schedule}"

    
class Schedule(models.Model):
    from_date = models.DateField("From")
    to_date = models.DateField("To")
    description = models.TextField("Description")
    quota = models.SmallIntegerField("Quota", default=1)
    cordon = models.ForeignKey('Cordon', related_name='schedules', on_delete=models.CASCADE)

    @property
    def name(self):
        return self.__str__()
    @property
    def link(self):
        return 'https://google.com'
    
    def __str__(self):
        return f"<Schedule> {self.from_date}-{self.to_date} : {self.cordon.name}"

    @classmethod
    def offer(cls):
        return cls.objects.annotate(num_users=models.Count('users')).filter(num_users__lt=models.F('quota'))

class Stage(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='stages', on_delete=models.CASCADE)
    wiki = models.ForeignKey('Wiki', related_name='stages', on_delete=models.CASCADE)
    status = models.CharField("Status", max_length=250)

    def __str__(self):
        return f"<Stage> {self.user.username} - {self.wiki.name}: {self.status}"


class Wiki(models.Model):
    name = models.CharField("Name", max_length=20)
    link = models.URLField("Link")
    wiki = models.URLField("Wiki")
    description = models.TextField("Description")
    
    def __str__(self):
        return f"<Wiki> {self.name}: {self.wiki}"