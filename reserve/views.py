from rest_framework import status, viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.decorators import action

from django.contrib.auth import get_user_model

from .serializers import StageSerializer, CordonDetailedSerializer, ScheduleSerializer, WikiSerializer, TaskSerializer
from .models import Cordon, Schedule, Stage, Task, Wiki

# Create your views here.
class CordonViewSet(viewsets.ViewSet):
    lookup_field = 'telegram_id'

    def list(self, request):
        queryset = Cordon.objects.all()
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
        except:
            return Response({"error": "No such user"},
                            status=status.HTTP_400_BAD_REQUEST)
        queryset = user.cordon
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(queryset, many=False)
        return Response(serializer.data)

    def update(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
            cordon = Cordon.objects.get(**request.data)
        except:
            return Response({"error": "No such user or cordon"},
                            status=status.HTTP_400_BAD_REQUEST)
        user.cordon = cordon
        user.save()
        queryset = cordon
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(queryset, many=False)
        return Response(serializer.data)
    
    def get_serializer_class(self):
        # if self.action == 'retrieve':
        #     return CordonDetailedSerializer
        return CordonDetailedSerializer # I dont' know what you want for create/destroy/update. 

class ScheduleViewSet(viewsets.ViewSet):
    lookup_field = 'telegram_id'

    def list(self, request):
        queryset = Schedule.offer()
        serializer = ScheduleSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
        except:
            return Response({"error": "No such user"},
                            status=status.HTTP_400_BAD_REQUEST)
        queryset = user.schedule
        serializer = ScheduleSerializer(queryset, many=False)
        return Response(serializer.data)

    def update(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
            schedule = Schedule.objects.get(**request.data)
        except:
            return Response({"error": "Error"},
                            status=status.HTTP_400_BAD_REQUEST)
        user.schedule = schedule
        user.save()
        serializer = ScheduleSerializer(schedule, many=False)
        return Response(serializer.data)

class WikiViewSet(viewsets.ViewSet):
    lookup_field = 'telegram_id'

    def list(self, request):
        queryset = Wiki.objects.all()
        serializer = WikiSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
        except:
            return Response({"error": "No such user"},
                            status=status.HTTP_400_BAD_REQUEST)
        queryset = Stage.objects.filter(user=user)
        serializer = StageSerializer(queryset, many=True)
        return Response(serializer.data)
    
class TaskViewSet(viewsets.ViewSet):
    lookup_field = 'telegram_id'

    def list(self, request):
        queryset = Task.objects.all()
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, telegram_id=None):
        try:
            user = get_user_model().objects.get(telegram_id=telegram_id)
        except:
            return Response({"error": "No such user"},
                            status=status.HTTP_400_BAD_REQUEST)
        queryset = user.task
        serializer = TaskSerializer(queryset, many=False)
        return Response(serializer.data)
    