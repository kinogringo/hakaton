from django.contrib import admin
from .models import Cordon, Schedule, Stage, Wiki, Task

@admin.register(Cordon)
class CordonAdmin(admin.ModelAdmin):
    list_display = ('name', 'link')

@admin.register(Wiki)
class WikiAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'wiki', 'description')

@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('from_date', 'to_date', 'cordon', 'quota')

@admin.register(Stage)
class StageAdmin(admin.ModelAdmin):
    list_display = ('user', 'wiki', 'status')

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('schedule', 'description')